#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct struct_two_ints {
  int integer1;
  int integer2;
};


void banner () {
printf(" _ |  /     _ /  /        __| _)              |           _|               |   _)                    _ |  /     _ |  /   \n"); 
printf("  _| /_ )   __) /_' |   \\__ \\  |   ` \\   _ \\  |   -_)     _| _| _` |   _|   _|  |   _ \\    \\  (_-<    _| /_ )    _| /_' |\n"); 
printf("   _/ __|     _/   _|   ____/ _| _|_|_| .__/ _| \\___|   _| _| \\__,_| \\__| \\__| _| \\___/ _| _| ___/     _/ __|     _/   _|\n"); 
printf("                                       _|                                                                                \n\n"); 
printf("THIS PROGRAM WILL ADDITION OR SUBSTRACT 2 FRACTIONS OF POSITIVE VALUES.\n");
printf("MIN VALUE : 0/X, MAX VALUE : 99/1\n\n");
}


char parse_operation_input(char *operation) {
  int is_addition;
  int is_substraction;
  is_addition = strcmp(operation, "addition\n");
  if (is_addition == 0) {
    return '+';
  }
  is_substraction = strcmp(operation, "substraction\n");
  if (is_substraction == 0) {
    return '-';
  }
  else {
    printf("%s\n", "ERROR : invalid operation requested, valid operations are : addition and substraction");
    exit(1);
  }
}


int parse_integer_input() {
  char input[50];
  fgets(input, 51, stdin);
  if (strlen(input) < 2) {
    printf("%s\n", "ERROR : minimum length for numerators and denominators is 1 digit long");
    exit(1); 
  }
  if (strlen(input) > 3) {
    printf("%s\n", "ERROR : maximum length for numerators and denominators is 2 digits long");
    exit(1); 
  }
  int strtol_result;
  char *endptr;
  strtol_result = strtol(input, &endptr, 10);
    if (*endptr == '\n') {
      if (strtol_result < 0) {
        printf("%s\n", "ERROR : invalid negative integer value, this program only deal with positive fractions");
        exit(1);
      }
      else {
        return strtol_result;
        exit(0);
      }
    }
  else {
    printf("%s\n", "ERROR : invalid integer");
    exit(1); 
  } 
}


void display_fraction_equation(char arithmetic_operator, int numerator1, int denominator1, int numerator2, int denominator2) {
  printf("\n\n");
  printf("\t%d\t\t  \t\t%d\n", numerator1, numerator2);
  printf("\t-\t\t%c\t\t-\n", arithmetic_operator);
  printf("\t%d\t\t  \t\t%d\n", denominator1, denominator2);
}


struct struct_two_ints find_lowest_common_denominator(int denominator1, int denominator2) {
  int denom1;
  int denom2;
  bool answer_found = false;
  bool denominator_too_big = false;
  // 3 rows x 13 columns, we store the factor in row1, the possible denominators of frac1 in row2 and possible denominator of frac2 in row3
  int array2d[3][100];
  // initialize struct "factors" based on struct_two_ints
  struct struct_two_ints factors;
  if (denominator1 == denominator2) {
    printf("\n\n%s\n", "Denominators are the same, we can do an operation on these 2 fractions without changing the denominators and the numerators...");
    // pack 1 as values of the struct since denominators are the same (denominator not increased so factor of 1)
    factors.integer1 = 1;
    factors.integer2 = 1;
  }
  else {
    printf("\n\n%s\n", "Denominators are not the same, we need to find lowest common denominator to do an operation on these 2 fractions...");
    printf("\n%s\n\n", "getting possible lowest common denominators for denominator #1 and denominator #2");
    // if one of the denominator is bigger than 12, do not print time tables
    if ((denominator1 > 12) || (denominator2 > 12)) {
      printf("INFO : denominator is higher than 12, not printing multiplication tables.\n");
      denominator_too_big = true;
    }
    // if smaller, print the time tables
    else {
      printf("INFO : we look at our multiplication tables to find the lowest common denominator.\n\n");
      for (int i = 0; i < 13; i++) {
        printf("\t%d x %d = %d\t\t\t\t%d x %d = %d\n", denominator1, i, denominator1 * i, denominator2, i, denominator2 * i);
      }
    }
    // populate the 2d array
    for (int i = 0; i < 100; i++) {
      denom1 = denominator1 * i;
      denom2 = denominator2 * i;
      array2d[0][i] = i; 
      array2d[1][i] = denom1; 
      array2d[2][i] = denom2;
    }
    // compare denominators
    for (int i = 0; i < 100; i++) {
      denom1 = array2d[1][i];
      for (int j = 0; j < 100; j++) {
        denom2 = array2d[2][j];
        //printf("denom1 : %d, denom2 : %d\n", denom1, denom2);
        if ((denom1 == denom2) && (denom1 != 0)) {
          printf("\n\ncommon denominator found : %d\n\n", denom1);
          printf("\nto get these denominators, we multiplied the first denominator by %d and the second denominator by %d\n", array2d[0][i], array2d[0][j]);
          printf("\nthis means we need to do the same to the numerators or this will change the value of the fraction. (THIS IS NOT ALLOWED, changing the value of one of the fractions will give us a rubbish answer.)\n");
          printf("\nby multiplying the numerator and the denominator with the same value, we ensure the new fraction is EQUIVALENT (=) to the old one.\n");
          factors.integer1 = array2d[0][i];
          factors.integer2 = array2d[0][j];
          answer_found = true;
          break;
        }
      }
      if (answer_found == true) {
        break;
      }
      else {
        continue;
      }
    }
  }
  return factors;
}


int multiply_by_factor (int any, int factor) {
  int product = any * factor;
  return product;
}


int operation_on_numerators (char *operation, int num1, int num2) {
  int is_addition;
  int is_substraction;
  int total;
  is_addition = strcmp(operation, "addition\n");
  if (is_addition == 0) {
    total = num1 + num2;
  }
  is_substraction = strcmp(operation, "substraction\n");
  if (is_substraction == 0) {
    total = num1 - num2;
  }
  return total;
}


void answer(int total_num, int denom) {
  printf("\t%d\n", total_num);
  printf("\t-\n");
  printf("\t%d\n", denom);
}


struct struct_two_ints simplify(int num, int den) {
  struct struct_two_ints simplified_answer;
  if (den == 1) {
    printf("%s\n", "denominator is already 1, we can't simplify further here");
    simplified_answer.integer1 = num;
    simplified_answer.integer2 = den;
    return simplified_answer;
  }
  for (int i = den; i>1; i--) {
    if ((num%i == 0) && (den%i == 0)) {
      printf("HURRAY! we can divide both the numerator and the denominator by %d without a remainer\n", i);
        simplified_answer.integer1 = num / i;
        simplified_answer.integer2 = den / i;
        break;
    }
    else {
      printf("NOPE. we CAN'T divide both the numerator and the denominator by %d without a remainer\n", i);
      simplified_answer.integer1 = num;
      simplified_answer.integer2 = den;
      continue;
    }
  }
  return simplified_answer;
}


int main(int argc, char *argv[]) {
  char arithmetic_operator;
  char operation[13];
  int frac1_num;
  int frac1_den;
  int frac2_num;
  int frac2_den;
  int num_total;
  banner();
  printf("%s", "do you want to perform an addition or a substraction? : ");
  fgets(operation, 14, stdin);
  arithmetic_operator = parse_operation_input(operation);
  printf("%s", "Enter first fraction numerator : ");
  frac1_num = parse_integer_input();
  printf("%s", "Enter first fraction denominator : ");
  frac1_den = parse_integer_input();
  printf("%s", "Enter second fraction numerator : ");
  frac2_num = parse_integer_input();
  printf("%s", "Enter second fraction denominator : ");
  frac2_den = parse_integer_input();
  if ((frac1_den == 0) || (frac2_den == 0)) {
    printf("ERROR : we can’t have 0 in the denominator of a fraction. It’s just one of those weird things in math that we can’t do.\n");
    printf("ERROR : fraction undefined.\n");
    exit(1);
  }
  display_fraction_equation(arithmetic_operator, frac1_num, frac1_den, frac2_num, frac2_den);
  struct struct_two_ints factors = find_lowest_common_denominator(frac1_den, frac2_den);
  frac1_num = multiply_by_factor(frac1_num, factors.integer1);
  frac1_den = multiply_by_factor(frac1_den, factors.integer1);
  frac2_num = multiply_by_factor(frac2_num, factors.integer2);
  frac2_den = multiply_by_factor(frac2_den, factors.integer2);
  printf("\nThe equivalent fraction (with the multiplied numerator is) :\n");
  display_fraction_equation(arithmetic_operator, frac1_num, frac1_den, frac2_num, frac2_den);
  num_total = operation_on_numerators(operation, frac1_num, frac2_num);
  printf("\n\nanswer is:\n\n");
  answer(num_total, frac1_den);
  printf("\n\nattempting to simplify... We start by trying to divide both the demominator and the numerator by the value of the denominator and we go down\n");
  struct struct_two_ints simplified_answer = simplify(num_total, frac1_den);
  printf("simplified answer is:\n\n");
  num_total = simplified_answer.integer1;
  frac1_den = simplified_answer.integer2;
  answer(num_total, frac1_den);
  return 0;
}
