# Simple fractions

a little C program to help kids making addition and substractions on fractions

```
 _ |  /     _ /  /        __| _)              |           _|               |   _)                    _ |  /     _ |  /
  _| /_ )   __) /_' |   \__ \  |   ` \   _ \  |   -_)     _| _| _` |   _|   _|  |   _ \    \  (_-<    _| /_ )    _| /_' |
   _/ __|     _/   _|   ____/ _| _|_|_| .__/ _| \___|   _| _| \__,_| \__| \__| _| \___/ _| _| ___/     _/ __|     _/   _|
                                       _|

THIS PROGRAM WILL ADDITION OR SUBSTRACT 2 FRACTIONS OF POSITIVE VALUES.
MIN VALUE : 0/X, MAX VALUE : 99/1

do you want to perform an addition or a substraction? : addition
Enter first fraction numerator : 2
Enter first fraction denominator : 4
Enter second fraction numerator : 6
Enter second fraction denominator : 8


        2                               6
        -               +               -
        4                               8


Denominators are not the same, we need to find lowest common denominator to do an operation on these 2 fractions...

getting possible lowest common denominators for denominator #1 and denominator #2

INFO : we look at our multiplication tables to find the lowest common denominator.

        4 x 0 = 0                               8 x 0 = 0
        4 x 1 = 4                               8 x 1 = 8
        4 x 2 = 8                               8 x 2 = 16
        4 x 3 = 12                              8 x 3 = 24
        4 x 4 = 16                              8 x 4 = 32
        4 x 5 = 20                              8 x 5 = 40
        4 x 6 = 24                              8 x 6 = 48
        4 x 7 = 28                              8 x 7 = 56
        4 x 8 = 32                              8 x 8 = 64
        4 x 9 = 36                              8 x 9 = 72
        4 x 10 = 40                             8 x 10 = 80
        4 x 11 = 44                             8 x 11 = 88
        4 x 12 = 48                             8 x 12 = 96


common denominator found : 8


to get these denominators, we multiplied the first denominator by 2 and the second denominator by 1

this means we need to do the same to the numerators or this will change the value of the fraction. (THIS IS NOT ALLOWED, changing the value of one of the fractions will give us a rubbish answer.)

by multiplying the numerator and the denominator with the same value, we ensure the new fraction is EQUIVALENT (=) to the old one.

The equivalent fraction (with the multiplied numerator is) :


        4                               6
        -               +               -
        8                               8


answer is:

        10
        -
        8


attempting to simplify... We start by trying to divide both the demominator and the numerator by the value of the denominator and we go down
NOPE. we CAN'T divide both the numerator and the denominator by 8 without a remainer
NOPE. we CAN'T divide both the numerator and the denominator by 7 without a remainer
NOPE. we CAN'T divide both the numerator and the denominator by 6 without a remainer
NOPE. we CAN'T divide both the numerator and the denominator by 5 without a remainer
NOPE. we CAN'T divide both the numerator and the denominator by 4 without a remainer
NOPE. we CAN'T divide both the numerator and the denominator by 3 without a remainer
HURRAY! we can divide both the numerator and the denominator by 2 without a remainer
simplified answer is:

        5
        -
        4
```

*** -  this repository is mirrored from gitea ***
